#!/bin/bash

git gc --aggressive --prune=now

mv .git/config config

rm -rf .git

git init -b main

mv config .git/config

git lfs install

git add --all .

git commit -m "cleanup"

# git push github main --force

git push gitlab main --force

echo "################################################################"
echo "###################    cleanup  Done      ######################"
echo "################################################################"
